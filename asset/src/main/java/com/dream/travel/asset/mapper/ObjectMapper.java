package com.dream.travel.asset.mapper;

/**
 * @author Rezaul Hasan
 * @since 14-May-2019
 */
public interface ObjectMapper<T, R> {
  R map(T entity);
}

package com.dream.travel.asset.listener;

import com.dream.travel.asset.schema.BaseEntity;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.util.UUID;

/**
 * @author Rezaul Hasan
 * @since 14-May-2019
 */
public class TravelAuditingEntityListener {

  @PrePersist
  public void onPrePersist(BaseEntity baseEntity) {

    if (null == baseEntity.getUuid()) {
      setUUID(baseEntity);
    }
  }

  @PreUpdate
  public void onPreUpdate(BaseEntity baseEntity) {

    if (null == baseEntity.getUuid()) {
      setUUID(baseEntity);
    }
  }

  private BaseEntity setUUID(BaseEntity baseEntity) {

    return baseEntity.setUuid(UUID.randomUUID().toString().toUpperCase());
  }
}

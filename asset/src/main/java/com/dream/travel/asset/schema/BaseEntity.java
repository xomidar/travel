package com.dream.travel.asset.schema;

import com.dream.travel.asset.listener.TravelAuditingEntityListener;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author Rezaul Hasan
 * @since 14-May-2019
 */
@MappedSuperclass
@EntityListeners({AuditingEntityListener.class, TravelAuditingEntityListener.class})
public abstract class BaseEntity {

  @Column(unique = true, updatable = false)
  private String uuid;

  // ------------------------------------Create block
  @CreatedBy
  @Column(updatable = false)
  private String createdBy;

  @Access(AccessType.PROPERTY)
  @Column(name = "CREATE_DATE", updatable = false)
  private LocalDate createDate;

  @CreatedDate
  @Column(name = "CREATE_DATE_TIME", updatable = false)
  private LocalDateTime createDateTime;

  // ------------------------------------Update block

  @LastModifiedBy private String updatedBy;

  @LastModifiedDate
  @Column(name = "UPDATE_DATE")
  private LocalDateTime updateDate;

  @LastModifiedDate
  @Column(name = "UPDATE_DATE_TIME")
  private LocalDateTime updateDateTime;

  // region <Getter and Setter>

  public String getUuid() {
    return uuid;
  }

  public BaseEntity setUuid(String uuid) {
    this.uuid = uuid;
    return this;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public BaseEntity setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
    return this;
  }

  public LocalDate getCreateDate() {
    if (this.createDate == null && this.createDateTime != null) {
      this.createDate = createDateTime.toLocalDate();
    }
    return createDate;
  }

  public BaseEntity setCreateDate(LocalDate createDate) {
    this.createDate = createDate;
    return this;
  }

  public LocalDateTime getCreateDateTime() {
    return createDateTime;
  }

  public BaseEntity setCreateDateTime(LocalDateTime createDateTime) {
    this.createDateTime = createDateTime;
    return this;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public BaseEntity setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
    return this;
  }

  public LocalDateTime getUpdateDate() {
    return updateDate;
  }

  public BaseEntity setUpdateDate(LocalDateTime updateDate) {
    this.updateDate = updateDate;
    return this;
  }

  public LocalDateTime getUpdateDateTime() {
    return updateDateTime;
  }

  public BaseEntity setUpdateDateTime(LocalDateTime updateDateTime) {
    this.updateDateTime = updateDateTime;
    return this;
  }

  // endregion
}

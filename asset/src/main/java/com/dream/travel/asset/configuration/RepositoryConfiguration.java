package com.dream.travel.asset.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration("BaseRepositoryConfiguration")
@EnableTransactionManagement
@EnableJpaAuditing
public class RepositoryConfiguration {}

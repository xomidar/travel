#Database

**Setup:**

- Create database name 'travel' at MySQL
>CREATE DATABASE travel;

- Change database connection URL and credential at application.properties 


#Project

**Run:**

- Clone git repository.


    > git clone https://gitlab.com/xomidar/travel.git    

- Go to directory.


    > cd travel    

- Run gradle wrapper from the command line.


    > gradlew clean build -x test    
- Go to directory.


    > cd 'travel/user/build/libs'    
- Run application from the command line.


    > java -jar dream-travel-and-tours-0.0.1-SNAPSHOT.jar    
- Got to 'server:8080' from web browser.
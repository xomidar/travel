package com.dream.travel.domain;

/**
 * @author Rezaul Hasan
 * @since 15-May-2019
 */
public class Status {

  private Long id;

  private String message;

  private VisibilityScope visibilityScope;

  private String locationName;

  private String userEmail;

  private String userName;

  private String duration;

  // region <Getter and Setter>

  public Long getId() {
    return id;
  }

  public Status setId(Long id) {
    this.id = id;
    return this;
  }

  public String getMessage() {
    return message;
  }

  public Status setMessage(String message) {
    this.message = message;
    return this;
  }

  public VisibilityScope getVisibilityScope() {
    return visibilityScope;
  }

  public Status setVisibilityScope(VisibilityScope visibilityScope) {
    this.visibilityScope = visibilityScope;
    return this;
  }

  public String getLocationName() {
    return locationName;
  }

  public Status setLocationName(String locationName) {
    this.locationName = locationName;
    return this;
  }

  public String getUserEmail() {
    return userEmail;
  }

  public Status setUserEmail(String userEmail) {
    this.userEmail = userEmail;
    return this;
  }

  public String getUserName() {
    return userName;
  }

  public Status setUserName(String userName) {
    this.userName = userName;
    return this;
  }

  public String getDuration() {
    return duration;
  }

  public Status setDuration(String duration) {
    this.duration = duration;
    return this;
  }

  // endregion
}

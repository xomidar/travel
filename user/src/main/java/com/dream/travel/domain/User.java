package com.dream.travel.domain;

import com.dream.travel.annotation.CompareProperties;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

/**
 * @author Rezaul Hasan
 * @since 14-May-2019
 */
@CompareProperties.List({
  @CompareProperties(
      first = "password",
      second = "confirmPassword",
      message = "The password fields must match"),
  @CompareProperties(
      first = "email",
      second = "confirmEmail",
      message = "The email fields must match")
})
public class User {

  @NotEmpty private String firstName;

  @NotEmpty private String lastName;

  @NotEmpty private String password;

  @NotEmpty private String confirmPassword;

  @Email @NotEmpty private String email;

  @Email @NotEmpty private String confirmEmail;

  @AssertTrue private Boolean terms;

  // region <Getter and Setter>

  public String getFirstName() {
    return firstName;
  }

  public User setFirstName(String firstName) {
    this.firstName = firstName;
    return this;
  }

  public String getLastName() {
    return lastName;
  }

  public User setLastName(String lastName) {
    this.lastName = lastName;
    return this;
  }

  public String getPassword() {
    return password;
  }

  public User setPassword(String password) {
    this.password = password;
    return this;
  }

  public String getConfirmPassword() {
    return confirmPassword;
  }

  public User setConfirmPassword(String confirmPassword) {
    this.confirmPassword = confirmPassword;
    return this;
  }

  public String getEmail() {
    return email;
  }

  public User setEmail(String email) {
    this.email = email;
    return this;
  }

  public String getConfirmEmail() {
    return confirmEmail;
  }

  public User setConfirmEmail(String confirmEmail) {
    this.confirmEmail = confirmEmail;
    return this;
  }

  public Boolean getTerms() {
    return terms;
  }

  public User setTerms(Boolean terms) {
    this.terms = terms;
    return this;
  }

  // endregion
}

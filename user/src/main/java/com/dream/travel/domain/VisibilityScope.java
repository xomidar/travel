package com.dream.travel.domain;

/**
 * @author Rezaul Hasan
 * @since 15-May-2019
 */
public enum VisibilityScope {
  PUBLIC,
  PRIVATE,
  FRIENDS
}

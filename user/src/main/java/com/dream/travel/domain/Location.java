package com.dream.travel.domain;

/**
 * @author Rezaul Hasan
 * @since 15-May-2019
 */
public class Location {

  private Long id;

  private String name;

  // region <Getter and Setter>

  public Long getId() {
    return id;
  }

  public Location setId(Long id) {
    this.id = id;
    return this;
  }

  public String getName() {
    return name;
  }

  public Location setName(String name) {
    this.name = name;
    return this;
  }

  // endregion
}

package com.dream.travel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Rezaul Hasan
 * @since 14-May-2019
 */
@SpringBootApplication
public class DreamTravelAndToursApplication {

	public static void main(String[] args) {
		SpringApplication.run(DreamTravelAndToursApplication.class, args);
	}

}

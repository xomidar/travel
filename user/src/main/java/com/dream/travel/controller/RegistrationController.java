package com.dream.travel.controller;

import com.dream.travel.domain.User;
import com.dream.travel.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

/**
 * @author Rezaul Hasan
 * @since 14-May-2019
 */
@Controller
@RequestMapping("/registration")
public class RegistrationController {

  @Autowired private UserService userService;

  @ModelAttribute("user")
  public User user() {
    return new User();
  }

  @GetMapping
  public String showRegistrationForm(Model model) {
    return "registration";
  }

  @PostMapping
  public String registerUserAccount(
      @ModelAttribute("user") @Valid User user, BindingResult result) {

    if (userService.findByEmail(user.getEmail()).isPresent()) {

      result.rejectValue("email", null, "There is already an account registered with that email");
    }

    if (result.hasErrors()) {

      return "registration";
    }

    userService.save(user);

    return "redirect:/login?success";
  }
}

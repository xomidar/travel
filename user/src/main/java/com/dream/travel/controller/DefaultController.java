package com.dream.travel.controller;

import com.dream.travel.service.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

/**
 * @author Rezaul Hasan
 * @since 14-May-2019
 */
@Controller
public class DefaultController {

  @Autowired private StatusService statusService;

  @GetMapping("/")
  public String root(Model model, Principal principal) {

    model.addAttribute("status", statusService.findAllByUserEmail(principal.getName()));

    return "index";
  }

  @GetMapping("/login")
  public String login(Model model) {
    return "login";
  }
}

package com.dream.travel.controller;

import com.dream.travel.domain.Status;
import com.dream.travel.domain.VisibilityScope;
import com.dream.travel.service.LocationService;
import com.dream.travel.service.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.security.Principal;

/**
 * @author Rezaul Hasan
 * @since 15-May-2019
 */
@Controller
@RequestMapping("/status")
public class StatusController {

  @Autowired private StatusService statusService;
  @Autowired private LocationService locationService;

  @ModelAttribute("status")
  public Status status() {
    return new Status();
  }

  @GetMapping
  public String showRegistrationForm(Model model) {

    model.addAttribute("locations", locationService.findAll());
    model.addAttribute("visibilityScopes", VisibilityScope.values());

    return "status";
  }

  @PostMapping
  public String postStatus(
      @ModelAttribute("status") @Valid Status status, Principal principal, BindingResult result) {

    status.setUserEmail(principal.getName());

    if (result.hasErrors()) {

      return "status";
    }

    statusService.save(status);

    return "redirect:/";
  }
}

package com.dream.travel.schema;

/**
 * @author Rezaul Hasan
 * @since 14-May-2019
 */
class SchemaConstant {

  static final String POST_TABLE = "POST";
  static final String POST_SEQUENCE_NAME = "S_POST";

  static final String LOCATION_TABLE = "LOCATION";
  static final String LOCATION_SEQUENCE_NAME = "S_LOCATION";

  static final String USER_TABLE = "USER";
  static final String USER_SEQUENCE_NAME = "S_USER";

  static final String ROLE_TABLE = "ROLE";
  static final String ROLE_SEQUENCE_NAME = "S_ROLE";
}

package com.dream.travel.schema;

import com.dream.travel.asset.schema.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author Rezaul Hasan
 * @since 14-May-2019
 */
@Entity
@Table(name = SchemaConstant.USER_TABLE)
public class UserEntity extends BaseEntity implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "USER_ID_GENERATOR")
  @SequenceGenerator(
      name = "USER_ID_GENERATOR",
      allocationSize = 1,
      sequenceName = SchemaConstant.USER_SEQUENCE_NAME)
  private Long id;

  private String firstName;

  private String lastName;

  @Column(unique = true)
  private String email;

  private String password;

  @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @JoinTable(
      name = "USER_ROLES",
      joinColumns = @JoinColumn(name = "USER_ID", referencedColumnName = "ID"),
      inverseJoinColumns = @JoinColumn(name = "ROLE_ID", referencedColumnName = "ID"))
  private List<RoleEntity> roles;

  @OneToMany(mappedBy = "user")
  private List<StatusEntity> posts;

  // region <Getter and Setter>

  public Long getId() {
    return id;
  }

  public UserEntity setId(Long id) {
    this.id = id;
    return this;
  }

  public String getFirstName() {
    return firstName;
  }

  public UserEntity setFirstName(String firstName) {
    this.firstName = firstName;
    return this;
  }

  public String getLastName() {
    return lastName;
  }

  public UserEntity setLastName(String lastName) {
    this.lastName = lastName;
    return this;
  }

  public String getEmail() {
    return email;
  }

  public UserEntity setEmail(String email) {
    this.email = email;
    return this;
  }

  public String getPassword() {
    return password;
  }

  public UserEntity setPassword(String password) {
    this.password = password;
    return this;
  }

  public List<RoleEntity> getRoles() {
    return roles;
  }

  public UserEntity setRoles(List<RoleEntity> roles) {
    this.roles = roles;
    return this;
  }

  public List<StatusEntity> getPosts() {
    return posts;
  }

  public UserEntity setPosts(List<StatusEntity> posts) {
    this.posts = posts;
    return this;
  }

  // endregion
}

package com.dream.travel.schema;

import javax.persistence.*;
import java.util.List;

/**
 * @author Rezaul Hasan
 * @since 15-May-2019
 */
@Entity
@Table(name = SchemaConstant.LOCATION_TABLE)
public class LocationEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "LOCATION_ID_GENERATOR")
  @SequenceGenerator(
      name = "LOCATION_ID_GENERATOR",
      allocationSize = 1,
      sequenceName = SchemaConstant.LOCATION_SEQUENCE_NAME)
  private Long id;

  private String name;

  @OneToMany(mappedBy = "location")
  private List<StatusEntity> posts;

  // region <Getter and Setter>

  public Long getId() {
    return id;
  }

  public LocationEntity setId(Long id) {
    this.id = id;
    return this;
  }

  public String getName() {
    return name;
  }

  public LocationEntity setName(String name) {
    this.name = name;
    return this;
  }

  public List<StatusEntity> getPosts() {
    return posts;
  }

  public LocationEntity setPosts(List<StatusEntity> posts) {
    this.posts = posts;
    return this;
  }

  // endregion
}

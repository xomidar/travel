package com.dream.travel.schema;

import javax.persistence.*;

/**
 * @author Rezaul Hasan
 * @since 14-May-2019
 */
@Entity
@Table(name = SchemaConstant.ROLE_TABLE)
public class RoleEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "ROLE_ID_GENERATOR")
  @SequenceGenerator(
      name = "ROLE_ID_GENERATOR",
      allocationSize = 1,
      sequenceName = SchemaConstant.ROLE_SEQUENCE_NAME)
  private Long id;

  private String name;

  // region <Getter and Setter>

  public Long getId() {
    return id;
  }

  public RoleEntity setId(Long id) {
    this.id = id;
    return this;
  }

  public String getName() {
    return name;
  }

  public RoleEntity setName(String name) {
    this.name = name;
    return this;
  }

  // endregion
}

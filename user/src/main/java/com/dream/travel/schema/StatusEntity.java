package com.dream.travel.schema;

import com.dream.travel.asset.schema.BaseEntity;
import com.dream.travel.domain.VisibilityScope;

import javax.persistence.*;

/**
 * @author Rezaul Hasan
 * @since 15-May-2019
 */
@Entity
@Table(name = SchemaConstant.POST_TABLE)
public class StatusEntity extends BaseEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "POST_ID_GENERATOR")
  @SequenceGenerator(
      name = "POST_ID_GENERATOR",
      allocationSize = 1,
      sequenceName = SchemaConstant.POST_SEQUENCE_NAME)
  private Long id;

  private String message;

  @Enumerated(EnumType.STRING)
  private VisibilityScope visibilityScope = VisibilityScope.PUBLIC;

  @ManyToOne
  @JoinColumn(name = "LOCATION_ID")
  private LocationEntity location;

  @ManyToOne
  @JoinColumn(name = "USER_EMAIL", referencedColumnName = "EMAIL")
  private UserEntity user;

  // region <Getter and Setter>

  public Long getId() {
    return id;
  }

  public StatusEntity setId(Long id) {
    this.id = id;
    return this;
  }

  public String getMessage() {
    return message;
  }

  public StatusEntity setMessage(String message) {
    this.message = message;
    return this;
  }

  public LocationEntity getLocation() {
    return location;
  }

  public StatusEntity setLocation(LocationEntity location) {
    this.location = location;
    return this;
  }

  public UserEntity getUser() {
    return user;
  }

  public StatusEntity setUser(UserEntity user) {
    this.user = user;
    return this;
  }

  public VisibilityScope getVisibilityScope() {
    return visibilityScope;
  }

  public StatusEntity setVisibilityScope(VisibilityScope visibilityScope) {
    this.visibilityScope = visibilityScope;
    return this;
  }

  // endregion
}

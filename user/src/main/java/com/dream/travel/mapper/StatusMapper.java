package com.dream.travel.mapper;

import com.dream.travel.asset.mapper.ObjectMapper;
import com.dream.travel.domain.Status;
import com.dream.travel.repository.LocationRepository;
import com.dream.travel.repository.StatusRepository;
import com.dream.travel.repository.UserRepository;
import com.dream.travel.schema.StatusEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 * @author Rezaul Hasan
 * @since 15-May-2019
 */
@Component
public class StatusMapper {

  @Autowired private StatusRepository statusRepository;
  @Autowired private LocationRepository locationRepository;
  @Autowired private UserRepository userRepository;

  public ObjectMapper<Status, StatusEntity> domainToEntity() {

    return domain ->
        statusRepository
            .findById(null == domain.getId() ? 0 : domain.getId())
            .orElseGet(StatusEntity::new)
            // TODO Handle exception
            .setLocation(locationRepository.findByName(domain.getLocationName()).orElseGet(null))
            .setMessage(domain.getMessage())
            // TODO Handle exception
            .setUser(userRepository.findByEmail(domain.getUserEmail()).orElseGet(null))
            .setVisibilityScope(domain.getVisibilityScope());
  }

  public ObjectMapper<StatusEntity, Status> entityToDomain() {
    return entity -> {
      String duration =
          ChronoUnit.HOURS.between(entity.getCreateDateTime(), LocalDateTime.now()) > 24
              ? ChronoUnit.DAYS.between(entity.getCreateDateTime(), LocalDateTime.now())
                  + " days ago"
              : ChronoUnit.HOURS.between(entity.getCreateDateTime(), LocalDateTime.now())
                  + " hours ago";

      return new Status()
          .setId(entity.getId())
          .setLocationName(entity.getLocation().getName())
          .setMessage(entity.getMessage())
          .setUserEmail(entity.getUser().getEmail())
          .setVisibilityScope(entity.getVisibilityScope())
          .setUserName(entity.getUser().getFirstName() + " " + entity.getUser().getLastName())
          .setDuration(duration);
    };
  }
}

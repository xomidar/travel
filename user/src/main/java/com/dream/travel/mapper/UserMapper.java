package com.dream.travel.mapper;

import com.dream.travel.asset.mapper.ObjectMapper;
import com.dream.travel.domain.User;
import com.dream.travel.repository.UserRepository;
import com.dream.travel.schema.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * @author Rezaul Hasan
 * @since 14-May-2019
 */
@Component
public class UserMapper {

  @Autowired private BCryptPasswordEncoder passwordEncoder;

  @Autowired private UserRepository userRepository;

  public ObjectMapper<User, UserEntity> domainToEntity() {
    return domain ->
        userRepository
            .findByEmail(domain.getEmail())
            .orElseGet(UserEntity::new)
            .setFirstName(domain.getFirstName())
            .setLastName(domain.getLastName())
            .setEmail(domain.getEmail())
            .setPassword(passwordEncoder.encode(domain.getPassword()));
  }
}

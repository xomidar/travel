package com.dream.travel.mapper;

import com.dream.travel.asset.mapper.ObjectMapper;
import com.dream.travel.domain.Location;
import com.dream.travel.repository.LocationRepository;
import com.dream.travel.schema.LocationEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Rezaul Hasan
 * @since 15-May-2019
 */
@Component
public class LocationMapper {

  @Autowired private LocationRepository locationRepository;

  public ObjectMapper<Location, LocationEntity> domainToEntity() {

    return domain ->
        locationRepository
            .findById(domain.getId())
            .orElseGet(LocationEntity::new)
            .setName(domain.getName());
  }

  public ObjectMapper<LocationEntity, Location> entityToDomain() {

    return entity -> new Location().setId(entity.getId()).setName(entity.getName());
  }
}

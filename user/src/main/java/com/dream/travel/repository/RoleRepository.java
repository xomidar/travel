package com.dream.travel.repository;

import com.dream.travel.schema.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * @author Rezaul Hasan
 * @since 15-May-2019
 */
public interface RoleRepository extends JpaRepository<RoleEntity, Long> {

  Optional<RoleEntity> findByName(String name);
}

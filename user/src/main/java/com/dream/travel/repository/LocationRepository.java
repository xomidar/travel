package com.dream.travel.repository;

import com.dream.travel.schema.LocationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * @author Rezaul Hasan
 * @since 15-May-2019
 */
public interface LocationRepository extends JpaRepository<LocationEntity, Long> {

  Optional<LocationEntity> findByName(String name);
}

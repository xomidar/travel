package com.dream.travel.repository;

import com.dream.travel.domain.VisibilityScope;
import com.dream.travel.schema.StatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * @author Rezaul Hasan
 * @since 15-May-2019
 */
public interface StatusRepository extends JpaRepository<StatusEntity, Long> {

  Optional<StatusEntity> findByUser_Email(String email);

  List<StatusEntity> findByUser_EmailOrVisibilityScopeOrderByCreateDateTimeDesc(
      String email, VisibilityScope visibilityScope);
}

package com.dream.travel.service;

import com.dream.travel.domain.Status;

import java.util.List;
import java.util.Optional;

/**
 * @author Rezaul Hasan
 * @since 15-May-2019
 */
public interface StatusService {

  Optional<Status> findByUserEmail(String email);

  List<Status> findAllByUserEmail(String email);

  void save(Status status);
}

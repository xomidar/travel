package com.dream.travel.service;

import com.dream.travel.domain.User;
import com.dream.travel.schema.UserEntity;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Optional;

/**
 * @author Rezaul Hasan
 * @since 14-May-2019
 */
public interface UserService extends UserDetailsService {

  Optional<UserEntity> findByEmail(String email);

  UserEntity save(User registration);
}

package com.dream.travel.service;

import com.dream.travel.domain.Location;

import java.util.List;

/**
 * @author Rezaul Hasan
 * @since 15-May-2019
 */
public interface LocationService {

  List<Location> findAll();
}

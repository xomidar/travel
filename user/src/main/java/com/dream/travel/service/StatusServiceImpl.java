package com.dream.travel.service;

import com.dream.travel.domain.Status;
import com.dream.travel.domain.VisibilityScope;
import com.dream.travel.mapper.StatusMapper;
import com.dream.travel.repository.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Rezaul Hasan
 * @since 15-May-2019
 */
@Service
public class StatusServiceImpl implements StatusService {

  @Autowired private StatusRepository statusRepository;
  @Autowired private StatusMapper statusMapper;

  @Override
  public Optional<Status> findByUserEmail(String email) {

    return statusRepository.findByUser_Email(email).map(statusMapper.entityToDomain()::map);
  }

  @Override
  public List<Status> findAllByUserEmail(String email) {

    return statusRepository
        .findByUser_EmailOrVisibilityScopeOrderByCreateDateTimeDesc(email, VisibilityScope.PUBLIC)
        .stream()
        .map(statusMapper.entityToDomain()::map)
        .collect(Collectors.toList());
  }

  @Override
  public void save(Status status) {

    statusRepository.save(statusMapper.domainToEntity().map(status));
  }
}

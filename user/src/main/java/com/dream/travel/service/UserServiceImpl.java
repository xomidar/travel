package com.dream.travel.service;

import com.dream.travel.domain.User;
import com.dream.travel.mapper.UserMapper;
import com.dream.travel.repository.RoleRepository;
import com.dream.travel.repository.UserRepository;
import com.dream.travel.schema.RoleEntity;
import com.dream.travel.schema.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Rezaul Hasan
 * @since 14-May-2019
 */
@Service
public class UserServiceImpl implements UserService {

  @Autowired private UserRepository userRepository;
  @Autowired private RoleRepository roleRepository;
  @Autowired private UserMapper userMapper;

  public Optional<UserEntity> findByEmail(String email) {

    return userRepository.findByEmail(email);
  }

  public UserEntity save(User user) {

    UserEntity userEntity =
        userMapper
            .domainToEntity()
            .map(user)
            .setRoles(
                Arrays.asList(
                    roleRepository
                        .findByName("ADMIN")
                        .orElseGet(() -> new RoleEntity().setName("ADMIN"))));

    return userRepository.save(userEntity);
  }

  @Override
  public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

    UserEntity user =
        userRepository
            .findByEmail(email)
            .orElseThrow(() -> new UsernameNotFoundException("Invalid user email"));

    return new org.springframework.security.core.userdetails.User(
        user.getEmail(), user.getPassword(), mapRolesToAuthorities(user.getRoles()));
  }

  private Collection<? extends GrantedAuthority> mapRolesToAuthorities(List<RoleEntity> roles) {

    return roles.stream()
        .map(role -> new SimpleGrantedAuthority(role.getName()))
        .collect(Collectors.toList());
  }
}

package com.dream.travel.service;

import com.dream.travel.domain.Location;
import com.dream.travel.mapper.LocationMapper;
import com.dream.travel.repository.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Rezaul Hasan
 * @since 15-May-2019
 */
@Service
public class LocationServiceImpl implements LocationService {

  @Autowired private LocationRepository locationRepository;
  @Autowired private LocationMapper locationMapper;

  @Override
  public List<Location> findAll() {

    return locationRepository.findAll().stream()
        .map(locationMapper.entityToDomain()::map)
        .collect(Collectors.toList());
  }
}

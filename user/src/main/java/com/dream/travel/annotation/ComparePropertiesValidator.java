package com.dream.travel.annotation;

import org.apache.commons.beanutils.BeanUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author Rezaul Hasan
 * @since 15-May-2019
 */
public class ComparePropertiesValidator implements ConstraintValidator<CompareProperties, Object> {

  private String firstFieldName;
  private String secondFieldName;

  @Override
  public void initialize(final CompareProperties constraintAnnotation) {
    firstFieldName = constraintAnnotation.first();
    secondFieldName = constraintAnnotation.second();
  }

  @Override
  public boolean isValid(final Object value, final ConstraintValidatorContext context) {
    try {
      final Object firstObj = BeanUtils.getProperty(value, firstFieldName);
      final Object secondObj = BeanUtils.getProperty(value, secondFieldName);
      return firstObj == null && secondObj == null
          || firstObj != null && firstObj.equals(secondObj);
    } catch (final Exception ignore) {
    }
    return true;
  }
}
